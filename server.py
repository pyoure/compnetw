import socket
import random
import string
from _thread import *
import threading


ThreadCount = 0


def set_password(lenght):
    p = ''
    digits = string.digits
    letters = string.ascii_letters
    for i in range(int(lenght/2)):
        p += str(random.choice(digits))
    for i in range(int(lenght/2)):
        p += str(random.choice(letters))
    return p


def choise():
    cho = int(input('Please choose the variant of setting password: \n Write "2" to set it by yourself and "1" to use authomatic setup: '))
    if cho == 1:
        l = int(input("And now please enter length which has to be twin:"))
        if l % 2 == 0:
            return set_password(l)
        else:
            print("Lenght is not twin((")
            return choise()
    elif cho == 2:
        return set_password_handsmode()
    else:
        print("That's not a required number")
        return choise()


def set_password_handsmode():
    digits = string.digits
    letters = string.ascii_letters

    p = input("Please enter your password which has to be twin length and contain only digits in first half and only letters in second")
    if (len(p)%2 == 0) and p[:int(len(p)/2)].isnumeric() and p[int(len(p)/2):].isalpha():
        return p
    else:
        raise Exception("Entered password doesn't approach for requirements")


def multi_threaded_client(client_sock, password):
    len_of_password = len(password)
    client_sock.sendall(str(len_of_password).encode())
    while True:
        try:
            data = client_sock.recv(1024).decode()
        except ConnectionResetError:
            break

        if data == password:
            print("Yes, that's right")
            yes = str.encode('Right password')
            client_sock.sendall(yes)
            break

        print("Try another one")
        no = str.encode('Wrong password')
        try:
            client_sock.sendall(no)
        except BrokenPipeError:
            break
    client_sock.close()
    print('connection closed')


def main():
    serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, proto=0)
    serv_sock.bind(('', 53228))
    serv_sock.listen(10)

    password = choise()

    try:
        while True:
            client_sock, client_addr = serv_sock.accept()
            print('Connected by', client_addr)
            print(f'Password: {password}')
            threading.Thread(target=multi_threaded_client, args=(client_sock, password)).start()

    except Exception as e:
        print(e)



main()
