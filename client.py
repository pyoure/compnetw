import socket
import string
import time
import random


client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_sock.connect(('127.0.0.1', 53228))


def choise():
    choise = int(input('Please choose the variant of guessing password: \n Write "1" to guess by yourself and "2" to use authomatic search: '))

    length_of_password = client_sock.recv(1024).decode()
    print(f"Lenght of password: {length_of_password}")

    if choise == 1:
        hand_input()
    elif choise == 2:
        auto_search(length_of_password)
    else:
        print("That's not a required number")


def check_password(password):
    client_sock.sendall(str.encode(password))
    f = client_sock.recv(1024)

    if f.decode() == 'Right password':
        return True

    return False


def print_wrapper(password, num_turn):
    print(f'{num_turn} turn. Your suggestion is: {password}')
    if check_password(password):
        print("And that's right")
        return True
    print("But unfortunately password is another")
    return False


def hand_input():
    num_turn = 1
    while True:
        password = input("Please enter the number:")
        while not password:
            password = input()

        if print_wrapper(password, num_turn):
            return
        num_turn += 1


def auto_mode(length):
    length = int(length)
    result = ''
    for i in range(length / 2):
        rand_digit = random.choice(string.digits)
        rand_letter = random.choice(string.letters)
        result = rand_digit + result + rand_letter

    return result


def auto_search(length_of_password):
    num_turn = 1
    while True:
        password = auto_mode(length_of_password)
        if print_wrapper(password, num_turn):
            return
        num_turn += 1
try:
    choise()
except BrokenPipeError:
    print('Server was disconnected!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TUPIE')
except ValueError as e:
    print(e)

client_sock.close()
print('And now we disconnect from server')